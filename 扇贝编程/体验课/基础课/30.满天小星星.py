import turtle
from random import randint


def draw_star():
  # 补全画一颗星星的代码
  turtle.color('black')
  turtle.hideturtle()
  turtle.begin_fill()
  for i in range(5):
    turtle.forward(10)
    turtle.right(144)
  turtle.end_fill()


# 将 ??? 改成你想画的星星个数
for i in range(300):
  turtle.speed(0)
  turtle.penup()
  x = randint(-500, 500)
  y = randint(-1000, 1000)
  turtle.goto(x, y)
  turtle.pendown()
  draw_star()

turtle.penup()
turtle.goto(0, -130)
turtle.pendown()
turtle.write('一闪一闪亮晶晶',  font=('SimHei', 12, 'bold'))

