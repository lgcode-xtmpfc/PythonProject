def record(name, age, height, weight):
  print('欢迎小朋友们！')
  print('姓名：', name)
  print('年龄：', age)
  print('身高：', height, 'cm')
  print('体重：', weight, 'kg')

record(age=5, weight=21, name='佐藤正男', height=109)
# 输出：
# 欢迎小朋友们！
# 姓名： 佐藤正男
# 年龄： 5
# 身高： 109 cm
# 体重： 21 kg