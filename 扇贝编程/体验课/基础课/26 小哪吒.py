class Human:
  def __init__(self, name, heads, arms):
    self.name = name
    self.heads = heads
    self.arms = arms

  def __str__(self):
    return ('我叫{}，我有{}头{}臂'.format(self.name, self.heads, self.arms))

nezha = Human('哪吒', 3, 6)
print(nezha)