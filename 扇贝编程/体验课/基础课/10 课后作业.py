ending_normal = '小红帽没有打怪，采采蘑菇就回了家'
ending_wolf = '小红帽使用技能「荆棘术」打败了灰狼'
ending_dragon_1 = '小红帽使用技能「火球术」打败了黑龙'
ending_dragon_2 = '小红帽使用技能「隐形术」躲过了黑龙，安全回到家中'

answer_1 = input('小红帽探险时遇到敌人了吗？回答有/没有：')
if answer_1 == '有':
    answer_2 = input('小红帽遇见的敌人是谁？回答灰狼/黑龙')
    if answer_2 == '灰狼':
        print(ending_wolf)
    elif answer_2 == '黑龙':
        answer_3 = input('小红帽带上万能魔杖了吗？回答带了/没带')
        if answer_3 == '带了':
            print(ending_dragon_1)
        else:
            print(ending_dragon_2)
   
else:
  print(ending_normal)

