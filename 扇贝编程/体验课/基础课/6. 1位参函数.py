def record(name, age, height, weight):
  print('欢迎小朋友们！')
  print('姓名：', name)
  print('年龄：', age)
  print('身高：', height, 'cm')
  print('体重：', weight, 'kg')


# 下面开始调用定义的函数，传入关键信息。
record('野原新之助', 5, 110, 22)
record('风间彻', 5, 112, 21)
# 输出：
# 欢迎小朋友们！
# 姓名： 野原新之助
# 年龄： 5
# 身高： 110 cm
# 体重： 22 kg
# 欢迎小朋友们！
# 姓名： 风间彻
# 年龄： 5
# 身高： 112 cm
# 体重： 21 kg
