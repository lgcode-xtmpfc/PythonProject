import requests
import random
import time
from bs4 import BeautifulSoup


def get_douban_books(url, proxies):
  headers = {
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
  }
  # 使用代理爬取数据
  res = requests.get(url, proxies=proxies, headers=headers)
  soup = BeautifulSoup(res.text, 'html.parser')
  items = soup.find_all('div', class_='pl2')
  for i in items:
    tag = i.find('a')
    name = tag['title']
    link = tag['href']
    print(name, link)


url = 'https://book.douban.com/top250?start={}'
urls = [url.format(num * 25) for num in range(10)]
# IP 代理池（瞎写的并没有用）
proxies_list = [
    {
        "http": "http://10.10.1.10:3128",
        "https": "http://10.10.1.10:1080",
    },
    {
        "http": "http://10.10.1.11:3128",
        "https": "http://10.10.1.11:1080",
    },
    {
        "http": "http://10.10.1.12:3128",
        "https": "http://10.10.1.12:1080",
    }
]
for i in urls:
  # 从 IP 代理池中随机选择一个
  proxies = random.choice(proxies_list)
  get_douban_books(i, proxies)
  time.sleep(1)
