import requests
import time
from bs4 import BeautifulSoup

# 将获取豆瓣读书数据的代码封装成函数


def get_douban_books(url):
  headers = {
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
  }
  res = requests.get(url, headers=headers)
  soup = BeautifulSoup(res.text, 'html.parser')
  items = soup.find_all('div', class_='pl2')
  for i in items:
    tag = i.find('a')
    name = tag['title']
    link = tag['href']
    print(name, link)


url = 'https://book.douban.com/top250?start={}'
urls = [url.format(num * 25) for num in range(10)]
for item in urls:
  get_douban_books(item)
  # 暂停 1 秒防止访问太快被封
  time.sleep(1)
