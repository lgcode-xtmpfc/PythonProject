import requests
from bs4 import BeautifulSoup

headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
}
res = requests.get('https://book.douban.com/top250', headers=headers)
soup = BeautifulSoup(res.text, 'html.parser')
items = soup.find_all('div', class_='pl2')
for i in items:
  tag = i.find('a')
  # 提取书名
  name = tag['title']
  # 提取链接
  link = tag['href']
  print(name, link)
# 输出：
# 追风筝的人 https://book.douban.com/subject/1770782/
# 解忧杂货店 https://book.douban.com/subject/25862578/
# 小王子 https://book.douban.com/subject/1084336/
# 白夜行 https://book.douban.com/subject/3259440/
# 活着 https://book.douban.com/subject/4913064/
# 嫌疑人X的献身 https://book.douban.com/subject/3211779/

print("-----------------")
headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
}
res = requests.get('https://book.douban.com/top250', headers=headers)
soup = BeautifulSoup(res.text, 'html.parser')
items = soup.find_all('div', class_='pl2')
for i in items:
  tag = i.find('a')
  # 去掉空格和换行符
  name = ''.join(tag.text.split())
  link = tag['href']
  print(name, link)

print("-----------------")
headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
}
res = requests.get('https://movie.douban.com/top250', headers=headers)
soup = BeautifulSoup(res.text, 'html.parser')
items = soup.find_all('div', class_='hd')
for i in items:
  tag = i.find('a')
  # 提取书名
  name = tag.find(class_='title').text
  # 提取链接
  link = tag['href']
  print(name, link)
# 输出：

def xiazai (bds,url,res,items,name):
    headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
    }
    res = requests.get('https://book.douban.com/top250', headers=headers)
    soup = BeautifulSoup(res.text, 'html.parser')
    items = soup.find_all('div', class_='pl2')
    for i in items:
        tag = i.find('a')
        # 提取书名
        name = tag['title']
        # 提取链接
        link = tag['href']
        print(name, link)
