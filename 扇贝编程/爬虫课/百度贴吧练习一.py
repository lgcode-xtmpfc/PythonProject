#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/4/28 21:35
# @Author  : Jerry
# @File    : 百度贴吧练习一.py
# @Software: PyCharm

# 需求:输入要爬取贴吧的名称，在输入爬取的起始页和终止页,把每一页保存到本地

# 分析:1.找url的规律
# https://tieba.baidu.com/f?kw=%E5%A6%B9%E5%AD%90&pn=0 第一页
# https://tieba.baidu.com/f?kw=%E5%A6%B9%E5%AD%90&pn=50第二页
# https://tieba.baidu.com/f?kw=%E5%A6%B9%E5%AD%90&pn=100 第三页
# 页数的规律 pn = (当前页数 - 1)*50
# 2.获取网页的内容
# 3.保存数据

import random
import urllib.request
import urllib.parse

# 随机获取一个ua
headers_list = [{ 'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'},{'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11'}]

headers = random.choice(headers_list)

name = input('请输入贴吧名:')

start = int(input('请输入起始页:'))

end = int(input('请输入结束页:'))

# 对贴吧name进行编码
kw = {'kw':name}

kw = urllib.parse.urlencode(kw)

# 拼接url 发请求 或响应 保存数据

for i in range(start,end+1):

    # 拼接url
    pn = (i - 1)*50
    baseurl = 'https://tieba.baidu.com/f?'

    url = baseurl + kw + '&pn=' + str(pn)


    # 发起请求
    req = urllib.request.Request(url,headers=headers)

    # print(url)
    res = urllib.request.urlopen(req)


    html = res.read().decode('utf-8')


    # 写入文件
    filename = '第' + str(i) + '页.html'

    with open(filename,'w',encoding='utf-8') as f:

        print('正在爬取第%d页'%i)
        f.write(html)
