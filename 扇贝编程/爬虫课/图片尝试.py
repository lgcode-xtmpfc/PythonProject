import requests
import time
import random
from bs4 import BeautifulSoup



# # 获取图片数据
# url = ('https://pakutaso.cdn.rabify.me/shared/img/thumb/cryptocurrency{}.jpg?d=500')
# urls = [url.format(1400+num) for num in range(100)]
# # 以二进制写入的方式打开一个名为 info.jpg 的文件
# for x in urls:
#     with open('{}.jpg', 'wb').format(x) as file:
# # 将数据的二进制形式写入文件中
#         file.write(res.content)
# # 将获取豆瓣读书数据的代码封装成函数
#   def get_douban_books(url):
#     headers = {
#       'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
#     }
#     res = requests.get(url, headers=headers)
#     for x in res:
#       open(x + '.jpg', 'wb').write(res.content)
#
#

#   for item in urls:
#     get_douban_books(item)
#     # 暂停 1 秒防止访问太快被封
#     time.sleep(1)


# 随机获取一个ua
headers_list = [{'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36'}, {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11'}]

headers = random.choice(headers_list)

name = input('请输入贴吧名:')

start = int(input('请输入起始页:'))

end = int(input('请输入结束页:'))

# 对贴吧name进行编码
kw = {'kw': name}

kw = urllib.parse.urlencode(kw)

# 拼接url 发请求 或响应 保存数据

for i in range(start, end+1):

    # 拼接url
    pn = (i - 1)*50
    baseurl = 'https: // pakutaso.cdn.rabify.me/shared/img/thumb/cryptocurrency'
    y = 1409 + [x * x for x in range(1, 11)]
    weib = '.jpg?d = 500'
    url = baseurl + y + weib  

    # 发起请求
    req = urllib.request.Request(url, headers=headers)
    

    # print(url)
    res = urllib.request.urlopen(req)

    html = res.read().decode('utf-8')
    ]

    # 写入文件
    filename = '第' + str(i) + '页.html'

    with open(filename, 'w', encoding='utf-8') as f:

        print('正在爬取第%d页' % i)
        f.write(html)
