import urllib.request
import urllib.parse

baidu = 'https://www.baidu.com/s?'

name = input('请输入你要搜索的内容：')

wd = {'wd': name}
name = urllib.parse.urlencode(wd)
url = baidu + name

print(url)

headers = {
     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36','Cookie':'BIDUPSID=4FA733ACE6D4F90A52D99F5F363CD85A; PSTM=1576929032; sug=3; sugstore=0; ORIGIN=0; bdime=0; BAIDUID=4FA733ACE6D4F90A83BE743C46630339:SL=0:NR=10:FG=1; BD_UPN=12314753; BDUSS=VFPUlFGU0RER1ZtcDdTR1lPek5vcEdZcUMtRFNNUkN3VHZTb35Cb1hRR0duOHRlRVFBQUFBJCQAAAAAAAAAAAEAAADzvSajSjdnaGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIYSpF6GEqReR; MCITY=-158%3A; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; H_PS_PSSID=; H_PS_645EC=9e896%2FhdaWy7uBsYfIeQKxjhCoWB1I56kSiZk45G7DyeSZsVpqa6Fx3Np2k; COOKIE_SESSION=3052_0_9_9_86_16_0_0_9_8_15_0_0_0_10_0_1588076550_0_1588079592%7C9%235669_143_1586614174%7C9'
}

#创建请求对象
req = urllib.request.Request(url,headers=headers)
#获取响应对象
res = urllib.request.urlopen(req)
#读取响应对象
html = res.read().decode('utf-8')
#写入文件
with open('结果.html','w',encoding='utf-8') as f:
    f.write(html)