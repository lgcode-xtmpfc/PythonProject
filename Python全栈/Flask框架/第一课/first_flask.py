# @ Time    : 2020/4/3 20:36
# @ Author  : JuRan

from flask import Flask
# from flask import config
# import config
from flask import request

app = Flask(__name__)

# https://www.baidu.com/
# 装饰器   http://127.0.0.1:5000/  URL
@app.route("/")
def hello_wrold():
    a = 1
    b = 0
    # c = a/b
    return "hello world"

@app.route("/juran/")
def hello_juran():
    return "这是我的第一个flask页面"


@app.route("/list/<int:aid>")   # /list/aid  http://127.0.0.1:5000/list/2
def article_list(aid):
    return "这是第{}篇文章".format(aid)

@app.route("/list/<path:aid>")
def article_detail(aid):
    return "detial-这是第{}篇文章".format(aid)

# article  blog

@app.route("/<any(article, blog):url_path>/")
def item(url_path):
    return url_path


# https://www.baidu.com/s?wd=python
@app.route("/wd")
def baidu():
    return request.args.get("name")

if __name__ == '__main__':
    # app.debug = True
    # debug=True
    # config    配置
    # app.config.update()
    # print(type(app.config))
    # print(isinstance(app.config, dict))
    # app.config.update(DEBUG=True)

    # 配置文件
    # app.config.from_object(config)
    # app.config.from_object('config')
    # app.config.from_pyfile('config.py', silent=True)
    app.config.from_pyfile('config.ini', silent=True)
    # debug=True
    app.run()





