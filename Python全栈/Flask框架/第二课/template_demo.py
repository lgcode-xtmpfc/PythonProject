# @ Time    : 2020/4/8 21:23
# @ Author  : JuRan
from flask import Flask, render_template

# , template_folder=r"./demo"
app = Flask(__name__)


@app.route("/")
def index():
    # 默认会从templates目录下面找模板文件
    # return render_template("index.html", username="逻辑教育", age=18, home="bbq")

    context = {
        "username": "逻辑教育",
        "age": 18,
        "books": {
            "Python": 33,
            "Java": 23,
            "PHP": 25
        },
        "book": ["Python", "PHP", "Java"]
    }
    # username="逻辑教育"
    return render_template("index.html", **context)

@app.route("/profile/")
def profile():
    return render_template("profile/user.html")


if __name__ == '__main__':
    app.run(debug=True, port=8000)

