# @ Time    : 2020/4/8 20:06
# @ Author  : JuRan

from flask import Flask, url_for, request, redirect, Response
from flask import make_response


app = Flask(__name__)


@app.route("/")
def index():
    # /article/2/  只传一个aid
    # 根据函数的名字,进行反转 得到函数对应的路由 重定向
    # /article/2/?page=2  aid=2, page=2
    print(url_for("article_list", aid=1000, page=2, t=123))
    return "hello world"


# http://127.0.0.1:5000/article/2/
@app.route("/article/<aid>/")
def article_list(aid):
    return "article list {}".format(aid)


@app.route("/detail/<did>/")
def article_detail(did):
    # print(url_for("index"))

    # /?next=%2F
    print(url_for("index", next="/"))
    return "article detail {}".format(did)


# 默认都是接受GET请求
# @app.route("/login/", methods=['GET', 'POST'])
# def login():
    # GET    参数直接在URL中
    # POST   参数没有直接体现在URL地址中
    # print(type(request.args))
    # GET请求接受参数
    # print(request.args.get('username'))
    # POST请求接受参数
    # print(request.form.get("name"))
    # return "login"

# 页面重定向  登录
@app.route("/signup/")
def login():
    return "login"

@app.route("/profile/")
def profile():
    name = request.args.get("name")

    if name:
        return name
    else:
        # 重定向到登陆页面
        return redirect(url_for("login"), code=301)


@app.route("/about/")
def about():
    # return "juran"
    # return ['123']
    # return {"name": "juran"}
    # return ('name', "python")[1]
    # return (["python"], "java")
    # return Response("关于我们", status=200, mimetype="text/html")
    # return "关于我们", 200
    return make_response("关于我们")

if __name__ == '__main__':
    app.run(debug=True)


