from django.contrib import admin
from django.urls import path,re_path    #导入re_path函数
from django.views.static import serve   #导入serve
from django.conf import settings        #导入settings
from django.conf.urls import include    #导入include函数
from newsApp import views

urlpatterns = [
    path('qyxw/<pageNo>',views.qyxw),
    path('addNews/',views.addNews),
    path('modNews/<id>',views.modNews),
    path('delNews/<id>/<curPageNo>',views.delNews),
    # path('upload',views.upload),
    re_path('media/(?P<path>.*)',serve,{'document_root':settings.MEDIA_ROOT}),
]
