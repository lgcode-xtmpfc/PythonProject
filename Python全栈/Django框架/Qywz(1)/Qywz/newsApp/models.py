from django.db import models

# Create your models here.
# 企业新闻 and 行业新闻
class news(models.Model):
    CHOICES = (('0','企业新闻'),('1','行业新闻'))
    id = models.AutoField(primary_key=True) # id，主键
    title = models.CharField(max_length=50) # 标题
    author = models.CharField(max_length=50) # 作者
    content = models.TextField() #新闻内容
    create_time = models.DateField(auto_now=True,null=True) # 发布日期
    classify = models.CharField(max_length=1,choices=CHOICES) #类别

# 通知公告
class notice(models.Model):
    id = models.AutoField(primary_key=True) # id，主键
    author = models.CharField(max_length=50) # 作者
    content = models.TextField() #通知内容
    create_time = models.DateField(auto_now=True,null=True) # 发布日期