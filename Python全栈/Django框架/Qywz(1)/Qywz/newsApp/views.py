from django.shortcuts import render
from newsApp.models import news,notice
from django import forms
from django.core.paginator import Paginator
from django.shortcuts import redirect

# Create your views here.
def qyxw(request,pageNo):
    newss = news.objects.all()
    pages = Paginator(newss,5)
    page = pages.get_page(pageNo)
    d = {'page':page}
    t = 'qyxw.html'
    return render(request,t,d)

class addNewsModelForm(forms.ModelForm):
    # publisher = forms.ModelChoiceField(label='出版社',queryset=publisher.objects.all(),empty_label=None)
    class Meta:
        model = news
        fields = ['id','title','author','classify','content']
        labels = {'id':'ID','title':'标题','author':'作者','classify':'分类','content':'新闻内容'}
      
def addNews(request):
    if request.method == 'POST':
        mfrm = addNewsModelForm(request.POST,request.FILES)
        if mfrm.is_valid():
            mfrm.save()
            msg = '数据添加成功!'
        else:
            msg = '数据校验未通过！'
    else:
        mfrm = addNewsModelForm()
        msg = '请输入新闻数据！'
    t = 'news.html'
    d = {'title':'添加新闻信息','url':'/news/addNews/','id':'','form':mfrm,'msg':msg}
    return render(request,t,d)

class modNewsModelForm(addNewsModelForm):
    id = forms.CharField(label='id',widget=forms.TextInput(attrs={'readonly':'readonly'}))

def modNews(request,id):
    xw = news.objects.get(id=id)
    if request.method == 'POST':
        mfrm = modNewsModelForm(request.POST,request.FILES,instance=xw)
        if mfrm.is_valid():
            mfrm.save()
            msg = '数据修改成功！'
        else:
            msg = '数据校验未通过！'
    else:
        mfrm = modNewsModelForm(instance=xw)
        msg = '请修改新闻数据！'
    t = 'news.html'
    d = {'title':'修改书本信息','url':'/news/modNews/','id':id,'form':mfrm,'msg':msg}
    return render(request,t,d)

def delNews(request,id,curPageNo):
    news.objects.get(id=id).delete()
    return redirect(qyxw,pageNo=curPageNo)

