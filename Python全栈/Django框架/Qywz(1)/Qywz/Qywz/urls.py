"""Qywz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path #导入re_path函数
from django.views.static import serve #导入serve
from django.conf import settings #导入settings
from django.conf.urls import include #导入include函数
from homeApp import views #导入首页应用的views模块

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index),#首页
    path('about/',include('aboutApp.urls')),#将所有以about开头的url都交给aboutApp下的urls.py处理
    path('news/',include('newsApp.urls')),#~~~
    path('contact/',include('contactApp.urls')),#~~~
    path('product/',include('productApp.urls')),#~~~
    re_path('media/(?P<path>.*)',serve,{'document_root':settings.MEDIA_ROOT}),
]
