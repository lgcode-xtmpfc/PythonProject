# Generated by Django 3.1.1 on 2020-10-12 11:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('productApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='create_time',
            field=models.DateField(null=True),
        ),
    ]
