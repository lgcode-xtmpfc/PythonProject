from django.db import models

# Create your models here.


class product(models.Model):
    id = models.AutoField(primary_key=True)  # id，主键
    name = models.CharField(max_length=50)  # 产品名
    img = models.ImageField(upload_to='img', blank=True, null=True)  # 照片
    intro = models.TextField()  # 内容介绍
    price = models.DecimalField(max_digits=5, decimal_places=2)  # 价格
    create_time = models.DateField(null=True)  # 生产日期
