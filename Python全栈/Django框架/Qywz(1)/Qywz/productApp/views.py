from django.shortcuts import redirect
from django.core.paginator import Paginator
from django import forms
from productApp.models import product
from django.shortcuts import render

# Create your views here.

def productlist(request, pageNo):
    products = product.objects.all()
    pages = Paginator(products, 5)
    page = pages.get_page(pageNo)
    d = {'page': page}
    t = 'productlist.html'
    return render(request, t, d)


class addproductsModelForm(forms.ModelForm):
    # publisher = forms.ModelChoiceField(label='出版社',queryset=publisher.objects.all(),empty_label=None)
    class Meta:
        model = product
        fields = ['id', 'name', 'img', 'intro', 'price','create_time']
        labels = {'id': 'ID', 'name': '产品名', 'img': '图片','intro':'介绍',
                  'price': '价格', 'create_time': '日期'}

def addproducts(request):
    if request.method == 'POST':
        mfrm = addproductsModelForm(request.POST, request.FILES)
        if mfrm.is_valid():
            mfrm.save()
            msg = '数据添加成功!'
        else:
            msg = '数据校验未通过！'
    else:
        mfrm = addproductsModelForm()
        msg = '请输入产品信息！'
    t = 'product.html'
    d = {'title':'添加信息','url':'/product/addproducts/','id':'','form':mfrm,'msg':msg}
    return render(request,t,d)


class modproductsModelForm(addproductsModelForm):
    id = forms.CharField(label='id',widget=forms.TextInput(attrs={'readonly':'readonly'}))


def modproducts(request, id):
    xw = product.objects.get(id=id)
    if request.method == 'POST':
        mfrm = modproductsModelForm(
            request.POST, request.FILES, instance=xw)
        if mfrm.is_valid():
            mfrm.save()
            msg = '数据修改成功！'
        else:
            msg = '数据校验未通过！'
    else:
        mfrm = modproductsModelForm(instance=xw)
        msg = '请修改产品数据！'
    t = 'product.html'
    d = {'title': '修改产品信息', 'url': '/products/modproducts/',
         'id': id, 'form': mfrm, 'msg': msg}
    return render(request,t,d)

def delproducts(request,id,curPageNo):
    product.objects.get(id=id).delete()
    return redirect(product,pageNo=curPageNo)
