from django.contrib import admin
from django.urls import path, re_path  # 导入re_path函数
from django.views.static import serve  # 导入serve
from django.conf import settings  # 导入settings
from django.conf.urls import include  # 导入include函数
from productApp import views

urlpatterns = [

    # path('cp1/<pageNo>', views.cp1),
    path('productlist/<pageNo>', views.productlist),
    path('addproducts/', views.addproducts),
    path('modproducts/<id>', views.modproducts),
    path('delproducts/<id>/<curPageNo>', views.delproducts),
    # path('upload', views.upload),
    re_path('media/(?P<path>.*)', serve,
            {'document_root': settings.MEDIA_ROOT}),

]
