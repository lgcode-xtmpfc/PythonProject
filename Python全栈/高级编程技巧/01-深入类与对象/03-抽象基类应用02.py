"""
强制子类重写
方法1 主动抛出异常
"""
# class CacheBase(object):
#     def dele(self):
#         raise NotImplementedError
#
#     def crea(self):
#         raise NotImplementedError
#
#
# class RedisBase(CacheBase):
#     """
#     1.子类 如果不重写 父类 方法 访问时  直接抛出异常
#     """
#
#     def crea(self):
#         print("create")
#
#
# r = RedisBase()
# r.crea()
# r.dele()


"""
强制子类重写
方法2 抽象基类实现
"""
import abc


# 注意：不是直接继承object 而是继承abc.ABCMeta
class CacheBase(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def dele(self):
        pass

    def crea(self):
        pass


class RedisBase(CacheBase):
    def dele(self):
        pass


r = RedisBase()
"""
抽象基类 ： 实例化的时候检测
主动抛出异常 ：调用时才会检测
"""
