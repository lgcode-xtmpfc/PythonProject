# a = [1,2]
# b = [2,3]
# c = (5,6)
# d = "abc"

# a.extend(b)
# a.extend(c)
# a.extend(d)
# print(a)

"""
鸭子类型 在运行之前 Cat,Dog都是在列表里面，当作变量
当运行时，加上()调用info() 才明确Cat是一个类
"""

class Cat(object):
    def info(self):
        print("i am cat")


class Dog(object):
    def info(self):
        print("i am dog")


class Duck(object):
    def info(self):
        print("i am duck")


animal_list = [Cat, Dog, Duck]  # 变量

for animal in animal_list:
    animal().info()


