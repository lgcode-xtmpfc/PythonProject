class Father(object):
    # 类属性
    cls_ins = 1

    def __init__(self, a, b):
        # 实例属性
        self.a = a
        self.b = b
        # self.cls_ins = 886


zs = Father(3, 4)
print(zs.cls_ins, zs.a, zs.b)  # 1 3 4

print(Father.cls_ins)

# print(Father.a)  # 报错

"""
对象 可以向上查找 可以访问到 类属性
类不能向下查找 所以 只能访问到类属性。 类名.实例属性  报错
"""
Father.cls_ins = 778  # 覆盖

zs.cls_ins = 886  # 相当于封装
print(Father.cls_ins)
print(zs.cls_ins)


class MyTest(object):
    ins = "cls_ins"

    def __init__(self):
        self.ins = "ins"


t = MyTest()
print(MyTest.ins)  # cls_ins
print(t.ins)  # ins

"""
当对象 自己有该实例属性的时候  就直接输出自己的
当对象 自己没有该属性的时候 才会向类 向上查找 
"""


# 棱形继承
class D(object):
    pass


class B(D):
    pass


class C(D):
    pass


class A(B, C):
    pass


print(A.__mro__)
