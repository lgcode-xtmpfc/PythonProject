"""
abc模块 abc.py  不要以abc为模块名称
"""
class Demo(object):
    def __init__(self,li):
        self.li = li

    def __len__(self):
        return len(self.li)


l = ["C","凌云","九折"]
d = Demo(l)
# print(d)     # <__main__.Demo object at 0x00000183FF03A2B0>
# print(len(d))  # 会触发__len__  return len(self.li)  所以 返回3


"""
如何判断 Demo中 是否 含有 __len__魔法方法
"""
# print(hasattr(d,"__len__"))  # 判断 d的内部 是否含有__len__   返回值为 布尔


"""
可以通过判断 d 是否是 Sized的子类  然后进一步判断 d这个对象的类 是否 含有 __len__方法
"""
from collections.abc import Sized

# 判断d 是否是 Sized 这个类
print(isinstance(d,Sized))    # True 说明d 是Sized 类型




