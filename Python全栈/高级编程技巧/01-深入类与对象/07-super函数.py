"""
super基础使用
"""
class A(object):
    def __init__(self):
        print("A")


class B(A):
    def __init__(self):
        print("B")
        # 重写父类的构造方法  再调用父类的方法
        super().__init__()


b = B()


"""
软编码与硬编码
"""
class Person(object):
    def __init__(self, name, age, weight):
        self.name = name
        self.age = age
        self.weight = weight

    def speak(self):
        print(f"{self.name}说:我{self.age}岁")


class Student(Person):
    def __init__(self, name, age, weight, grade):
        # 软编码 -> 父类的名字可以随便更改
        # super().__init__(name, age, weight)

        # 硬编码
        Person.__init__(self, name, age, weight)
        self.grade = grade

    def speak(self):
        print(f"{self.name}说:我{self.age}岁,我在{self.grade}年级")


hz = Student('hz', 20, 120, 2)
hz.speak()


"""
super函数的继承机制
"""
class A(object):
    def __init__(self):
        print("A")


class C(A):
    def __init__(self):
        print("B")
        super().__init__()


class B(A):
    def __init__(self):
        print("C")
        super().__init__()


class D(B,C):
    def __init__(self):
        print("D")
        super().__init__()


if __name__ == '__main__':
    d = D()
    print(D.__mro__)


