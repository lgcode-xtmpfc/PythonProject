class Perpon(object):
    name = "会太阳"


class Student(Perpon):
    def __init__(self, school_name):
        self.school_name = school_name


hty = Student("Logic_edu")
print(hty.__dict__)  # {'school_name': 'Logic_edu'}  当前对象的属性 {"属性":"属性的值"}

print(dir(hty))  # []  考虑到继承的成员
