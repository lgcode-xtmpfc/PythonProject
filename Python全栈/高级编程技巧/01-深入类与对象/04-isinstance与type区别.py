"""
isinstance 与 type 基础使用
"""
a = 1
b = "耗子"

print(isinstance(b, (int, str)))  # 返回值为布尔值  ()-->or
print(type(a))  # <class 'int'>  int


"""
考虑继承
"""
class Father(object):
    pass


class Son(Father):
    pass


ls = Son()
print(isinstance(ls, Son))  # True
print(isinstance(ls, Father))  # True  继承

print(type(ls) is Son)  # True
print(type(ls) is Father)  # False 并不考虑继承关系 主要 两个类

# print(f"Son_id:{id(Son)}，Father_id:{id(Father)}")

"""
is    是否引用同一个对象 id()
==    值
"""
