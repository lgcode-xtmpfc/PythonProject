"""
将共同代码抽出 强制子类 实现area方法
"""
import abc
from functools import total_ordering
import math


@total_ordering
class Shape(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def area_res(self):
        pass

    def __gt__(self, other):
        return self.area_res() > other.area_res()

    def __ge__(self, other):
        return self.area_res() >= other.area_res()


class Rect(Shape):
    def __init__(self, w, h):
        self.w = w
        self.h = h

    def area_res(self):
        return self.w * self.h

    def __str__(self):
        return f"({self.w},{self.h})"


class Circle(Shape):
    def __init__(self, r):
        self.r = r

    def area_res(self):
        return self.r ** 2 * math.pi


c = Circle(4)
r = Rect(1, 2)
print(c == r)
