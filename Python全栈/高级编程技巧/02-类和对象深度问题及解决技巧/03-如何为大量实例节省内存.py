import sys
import tracemalloc


class Player(object):
    def __init__(self, uid, name, status):
        self.uid = uid
        self.name = name
        self.status = status


class Player2(object):
    # 元组的内存消耗 比 列表 小  getsizeod()
    __slots__ = ("uid", "name", "status")

    def __init__(self, uid, name, status):
        self.uid = uid
        self.name = name
        self.status = status


# 主程序入口
if __name__ == '__main__':
    p1 = Player("1", "sige", 1)
    p2 = Player2("2", "tianhao", 1)

    # print(dir(p1))
    # print(dir(p2))

    # print(len(dir(p1)))  # 29    __dict__
    # print(len(dir(p2)))  # 28

    """
    集合 - --> 差集
    {'__weakref__', '__dict__'}
    - __weakref__ 弱引用
    - __dict__    动态绑定属性的
    """
    # print(set(dir(p1)) - set(dir(p2)))
    #
    # print(p1.__dict__)  # {}
    #
    # # 动态为p1添加属性
    # p1.__dict__["level"] = 1
    # print(p1.__dict__)
    #
    # # 动态为p1删除属性
    # del p1.__dict__["level"]
    # print(p1.__dict__)
    #
    # print(sys.getsizeof(p1.__dict__))  # 240

    """
    p2能吗？不能动态绑定属性  由于__slots__
    """
    # print(p2.__dict__)  # 报错
    # p2.level = 1  # 不能动态绑定 报错
    #
    # p2.__slots__ = (1,)  # 报错 只读

    """
    跟踪内存消耗
    """
    tracemalloc.start()  # 开始跟踪内存分配
    # pla_1 = [Player(1,2,3) for _ in range(10000)]       # size=1726 KiB  __dict__
    pla_2 = [Player2(1, 2, 3) for _ in range(10000)]  # size=711 KiB

    snapshot = tracemalloc.take_snapshot()  # 快照 当前内存分配
    top = snapshot.statistics("filename")  # 快照对象统计 监测文件

    for start in top[:10]:
        print(start)
