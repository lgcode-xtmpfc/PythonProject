import contextlib

"""
异常捕获复习
"""
# try:
#     print("123")
#     raise KeyError
# except KeyError as e:
#     print("KeyError")
# finally:
#     print("不论有无异常 都执行")

"""
异常捕获文件关闭
"""
# try:
#     f = open("test.txt", "w")
# except Exception as e:
#     print(e)
# finally:
#     print("i am close")
#     f.close()

"""
使用 with open()
"""
# with open("test.txt", "r") as f:
#     f.read()

"""
自定义类是否可以使用上下文管理器
"""
# class Demo(object):
#     def __enter__(self):
#         print("start")
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         """
#         参数 只有当 异常时 才有会值
#         :param exc_type: 异常类
#         :param exc_val: 异常值
#         :param exc_tb: 异常的跟踪信息
#         :return:
#         """
#         print(exc_type, exc_val, exc_tb)
#         print("end")
#
#     def test(self):
#         print("i am test")
#
#
# with Demo() as d:
#     # 调用 __enter__
#     # d.test()
#     d.t()
#     # 才会调用 __exit__方法 释放内存


"""
使用@contextlib.contextmanager实现上下文管理器
"""
@contextlib.contextmanager
def file_open(filename):
    print(" file open")
    # 函数加上yield关键字之后 整个函数变成 生成器
    yield {}
    print(" file close")


with file_open("test.txt") as f:
    # __enter__
    print(" file operation")
    # __exit__

"""
yield与return类似  程序遇到yield会将内容进行返回

不同之处
- yield再下次访问的时候 会接着 yield后面的代码继续执行
- return时直接返回退出函数
"""
