"""
列表推导式前身
"""
li = []
for i in range(10):
    print(i)
    if i % 2 == 0:
        li.append(i)
print(li)

"""
列表推导式
"""
print([i for i in range(10) if i % 2 == 0])

"""
列表推导式嵌套循环
"""
for i in range(10):
    for j in range(10):
        print(i, j)

print([(i, j) for i in range(10) for j in range(10)])
