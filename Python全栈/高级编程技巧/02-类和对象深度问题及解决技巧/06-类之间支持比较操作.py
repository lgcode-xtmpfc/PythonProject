"""
简单数值比较
"""
# a = 2
# b = 1
# print(a > 1)
# print(a.__gt__(b))       # gt --> >
#
# print("="*100)

"""
集合比较的的 包含关系
"""
# print({1,2,3} > {4})    # False  不是比较的大小
# print({1,2,3} > {1,3})  # True

from functools import total_ordering
import math

"""
矩形面积与矩形面积比较
"""
# 求矩形面积的类
@total_ordering
class Rect(object):
    def __init__(self, w, h):
        self.w = w
        self.h = h

    def area(self):
        return self.w * self.h

    def __str__(self):
        # 字符串格式化
        # .format()
        # f""  3.6版本以上支持
        return f"({self.w},{self.h})"

    def __gt__(self, other):
        return self.area() > other.area()

    def __ge__(self, other):
        return self.area() >= other.area()


# r1 = Rect(1,2)
# r2 = Rect(3,4)
#
# # print(r1)
# # print(r2)
#
# print(r1 > r2)    # 类与类之间比较 直接报错   添加 __gt__魔法方法 可以比较 结果:False
#
# print(r1 >= r2)
#
# print(r1 <= r2)


"""
⚪这个类 矩形面积与⚪的面积作比较
"""


# 圆的面积的类
@total_ordering
class Circle(object):
    def __init__(self, r):
        self.r = r

    def area(self):
        return self.r ** 2 * math.pi

    def __gt__(self, other):
        return self.area() > other.area()

    def __ge__(self, other):
        return self.area() >= other.area()


c = Circle(2)
r = Rect(2, 2)
print(c == r)
