# print(tuple([-1,1,2,0]))  # (-1, 1, 2, 0)
"""
实现前导
"""

# class IntTuple(tuple):
#     # 重写了父类的构造方法
#     def __init__(self, iterable):
#         # iterable里面元素 为整数且>0的值
#         # for i in iterable:
#         #     if isinstance(i, int) and i > 0:
#         #         super().__init__(i)  # 报错
#         print(self)  # (2, -2, 'jr', ['x', 'y'], 4)
#         """
#         说明 元组对象 再__init__构造方法之前 就已经创建好了
#         """
#
#
# int_tuple = IntTuple([2, -2, "jr", ["x", "y"], 4])
# print(int_tuple)


"""
__new__方法与__init__方法
"""
# class Demo(object):
#     def __new__(cls, *args, **kwargs):
#         print("__new__")
#         return object.__new__(cls)
#
#     def __init__(self):
#         print("__init__")
#
#
# # d = Demo()
# d = Demo.__new__(Demo)  # 创建对象
# Demo.__init__(d)  # 初始化


"""
列表与元组的创建方式
"""
# # print(list("abc"))
# li = list.__new__(list, "abc")
# print(li)  # []
# list.__init__(li, "abc")
# print(li)  # ['a', 'b', 'c']
#
# print(tuple("123"))
# tu = tuple.__new__(tuple, "123")
# print(tu)  # ('1', '2', '3') 注意:元组对象 再__new__方法的时候 就已经创建了
# tuple.__init__(tu, "123")
# print(tu)  # ('1', '2', '3')


"""
完整实现
"""
class IntTuple(tuple):
    # 重写了父类的构造方法
    def __new__(cls, iterable):
        # iterable里面元素 为整数且>0的值
        # for i in iterable:
        #     if isinstance(i,int) and i>0:

        # 列表推导式

        f = [i for i in iterable if isinstance(i, int) and i > 0]
        return super().__new__(cls, f)


int_tuple = IntTuple([2, -2, "jr", ["x", "y"], 4])
print(int_tuple)

"""
再创建tuple()的时候 加上过滤条件而已
"""
