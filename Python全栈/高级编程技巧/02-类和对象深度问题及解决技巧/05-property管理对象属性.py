"""
A.get_age() # 访问器
A.set_age() # 设置器

通过
A.age  访问
A.age = 20 设置

• 定义类AgeDemo
• 通过访问器访问年龄
• 通过设置器设置年龄
• 年龄不是int类型则主动抛出异常
"""
# class AgeDemo:
#     def __init__(self, age):
#         self.age = age
#
#     # 访问
#     def get_age(self):
#         return self.age
#
#     # 设置
#     def set_age(self, age):
#         if not isinstance(age, int):
#             raise TypeError("TypeError")
#         self.age = age
#
#
# a = AgeDemo(18)
# a.age = "20"    # 直接访问到属性
# print(a.age)    # 20 str  直接可以访问属性 不安全

"""
当赋值的时候 直接触发set_age()方法 
"""
class AgeDemo:
    def __init__(self, age):
        self.age = age

    # # 访问
    # def get_age(self):
    #     return self.age
    #
    # # 设置
    # def set_age(self, age):
    #     if not isinstance(age, int):
    #         raise TypeError("TypeError")
    #     self.age = age
    #
    # # property(getx, setx, delx, "I'm the 'x' property.")
    # age_pro = property(fget=get_age, fset=set_age)

    @property
    def age_test(self):
        return self.age

    @age_test.setter
    def age_test(self, age):
        if not isinstance(age, int):
            raise TypeError("TypeError")
        self.age = age


a = AgeDemo(18)
# a.age_pro = 20  # 赋值-->fset
# print(a.age_pro)  # 访问-->fget
#
# a.age_pro = "23"  # 赋值-->fset
# print(a.age_pro)  # 访问-->fget

a.age_test = 20
print(a.age_test)