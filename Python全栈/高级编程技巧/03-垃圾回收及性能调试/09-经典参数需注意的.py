def add(a,b):
    a += b
    a = a + b
    return a

# a = 1
# b = 2
# c = add(a,b)
#
# print(c)        # 3
# print(a,b)      # 1 2

# a = [1,2]
# b = [3,4]
#
# c = add(a,b)
#
# print(c)         # 1,2,3,4
# print(a,b)       # [1, 2, 3, 4]      3,4   可变的数据类型
"""
可变数据类型
a += b
a = a + b  不一样
"""

a = (1,2)
b = (3,4)
c = add(a,b)

print(c)
print(a,b)      # (1, 2) (3, 4)