from lib1 import Triangle
from lib2 import Rectangle
from lib3 import Circle
"""
getattr(s,"find") --> s.find

"""
shape1 = Triangle(3,4,5)
shape2 = Rectangle(4,6)
shape3 = Circle(2)

# 定义获取面积的函数
def get_area(shape):
    method_name = ["get_area","getArea","area"]

    for name in method_name:
        f = getattr(shape, name, None)  # shape.name

        # 如果f存在
        if f:
            # 调用f
            return f()


# print(get_area(shape1))
# print(get_area(shape2))
# print(get_area(shape3))

shape_list = [shape1,shape2,shape3]
area_li = list(map(get_area, shape_list))
print(area_li)

