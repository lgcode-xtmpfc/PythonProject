import os
import psutil
import gc
"""
引用次数 为0 的时候 一定会启用 垃圾回收吗
垃圾回收 一定是引用次数 为0的时候吗

充分非必要条件
"""
def show_info(start):
    # 获取当前进程id
    pid = os.getpid()

    # 获取当前堆成对象
    p = psutil.Process(pid)

    # 返回该对象的内存消耗
    info = p.memory_full_info()

    # 获取进程独自占用的物理内存 换算单位 MB
    memory = info.uss/1024/1024
    print(f"{start}一共占用{memory:.2f}MB")


def func():
    show_info("initial")

    a = [i for i in range(1000000)]   # 列表推导式 [0~9999999]
    b = [i for i in range(1000000)]  # 列表推导式 [0~9999999]
    show_info("created")

    # 相互引用
    a.append(b)
    b.append(a)


a = func()
gc.collect()            # 手动回收  如果引用次数 不为0时  手动回收 也是可以的
show_info("finished")
