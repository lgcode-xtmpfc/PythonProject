import os
import psutil

# def show_info(start):
#     # 获取当前进程id
#     pid = os.getpid()
#
#     # 获取当前堆成对象
#     p = psutil.Process(pid)
#
#     # 返回该对象的内存消耗
#     info = p.memory_full_info()
#
#     # 获取进程独自占用的物理内存 换算单位 MB
#     memory = info.uss/1024./1024
#     print(f"{start}一共占用{memory:.2f}MB")
#
#
# def func():
#     show_info("initial")
#     # 因为a是局部变量 局部变量在函数执行完毕 引用就会注销
#     a = [i for i in range(1000000)]   # 列表推导式 [0~9999999]
#     show_info("created")
#
#
# func()
# show_info("finished")


# def show_info(start):
#     # 获取当前进程id
#     pid = os.getpid()
#
#     # 获取当前堆成对象
#     p = psutil.Process(pid)
#
#     # 返回该对象的内存消耗
#     info = p.memory_full_info()
#
#     # 获取进程独自占用的物理内存 换算单位 MB
#     memory = info.uss/1024./1024
#     print(f"{start}一共占用{memory:.2f}MB")
#
#
# def func():
#     show_info("initial")
#     # a为全局变量的时候 即使函数体内代码执行完毕 返回到函数调用处时，对列表a的引用仍然时存在的
#     # 所以对象不会被垃圾回收，依然占有大量内存
#     global a
#     a = [i for i in range(1000000)]   # 列表推导式 [0~9999999]
#     show_info("created")
#
#
# func()
# show_info("finished")


def show_info(start):
    # 获取当前进程id
    pid = os.getpid()

    # 获取当前堆成对象
    p = psutil.Process(pid)

    # 返回该对象的内存消耗
    info = p.memory_full_info()

    # 获取进程独自占用的物理内存 换算单位 MB
    memory = info.uss/1024/1024
    print(f"{start}一共占用{memory:.2f}MB")


def func():
    show_info("initial")

    a = [i for i in range(1000000)]   # 列表推导式 [0~9999999]
    show_info("created")
    return a


a = func()
show_info("finished")