import sys
import gc

a = [1,2,3]  # 小整数对象池
print(sys.getrefcount(a))

# a = None  # 相当于将a变量指向了None

# del a  # 相当于自己把对象的引用删掉 本质上对象还没有进行回收
# gc.collect() # 手动启动回收
# print(a)


