import sys

# a = [1,2,3]
#
# # print(sys.getrefcount(a))   # a引用2次  a本身一次 getrefcount一次
#
# def func(a):
#     # 4次：a本身一次 函数调用一次 函数参数一次 getrefcount一次
#     print(sys.getrefcount(a))
#
# func(a)
#
# # a作为形参相当于函数体内的临时变量 所以 调用执行完毕会被释放掉 引用次数为0
# # 2 a本身一次 getrefcount一次
# print(sys.getrefcount(a))


a = [1,2,3]
print(sys.getrefcount(a))  #  2次

b = a
print(sys.getrefcount(a))   # 3次

c = b
d = c
print(sys.getrefcount(a))   # 5次

"""
1.getrefcount()只计一次引用次数 
2.变量赋值 b变量指向了a所在内存地址
"""
