#git工具 

## 简易的命令行入门教程:

### Git 全局设置:

git config --global user.name "xtmpfc"
git config --global user.email "2997057567@qq.com"

### 创建 git 仓库:

mkdir wsdc20201103
cd wsdc20201103
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/lgcode-xtmpfc/wsdc20201103.git
git push -u origin master

### 已有仓库?

cd existing\_git\_repo
git remote add origin https://gitee.com/lgcode-xtmpfc/wsdc20201103.git
git push -u origin master