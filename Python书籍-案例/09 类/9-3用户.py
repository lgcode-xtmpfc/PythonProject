# 9-3 用户：创建一个名为User的类，其中包含属性first_name和last_name，还有用户简介通常会存储的其他几个属性。
# 在类User中定义一个名为describe_user()的方法，它打印用户信息摘要；再定义一个名为greet_user()的方法，它向用户发出个性化的问候。
# 创建多个表示不同用户的实例，并对每个实例都调用上述两个方法。


class User:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def describe_user(self):
        print('用户摘要：{}与{}'.format(self.first_name, self.last_name))

    def greet_user(self):
        print('欢迎访问：{}与{}'.format(self.first_name, self.last_name))


User1 = User('艾克快餐', '餐饮行业的快餐')
print(User1.describe_user())
User1.describe_user()
User1.greet_user()
