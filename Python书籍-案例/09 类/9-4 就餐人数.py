# 9-4 就餐人数：在为完成练习9-1而编写的程序中，添加一个名为number_served的属性，并将其默认值设置为0。
# 根据这个类创建一个名为restaurant的实例；打印有多少人在这家餐馆就餐过，然后修改这个值并再次打印它。



class Restaurant:
    def __init__(self, restaurant_name, cuisine_type, number_served):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
        self.number_served = number_served

    def describ_resturant(self):
        print('本店名为{}，是专门做{}，有{}人来吃过'.format(self.restaurant_name, self.cuisine_type, self.number_served))

    def open_restaurant(self):
        print('餐馆正在营业')



restaurant = Restaurant('艾克快餐', '餐饮行业的快餐','1000')
print(restaurant.restaurant_name)
restaurant.describ_resturant()
restaurant.open_restaurant()

