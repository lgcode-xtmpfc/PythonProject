# 9-2 三家餐馆：根据你为完成练习9-1而编写的类创建三个实例，并对每个实例调用方法describe_restaurant()。

class Restaurant:
    def __init__(self, restaurant_name, cuisine_type):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type

    def describ_resturant(self):
        print('本店名为{}，是专门做{}'.format(self.restaurant_name, self.cuisine_type))

    def open_restaurant(self):
        print('餐馆正在营业')


restaurant = Restaurant('艾克快餐', '餐饮行业的快餐')
restaurant2 = Restaurant('邵士来', '餐饮行业的养生')
restaurant3 = Restaurant('壶师傅汤面', '餐饮行业的面馆')
print(restaurant.restaurant_name)
restaurant.describ_resturant()
restaurant.open_restaurant()
restaurant3.describ_resturant()
restaurant2.describ_resturant()